#!/bin/sh
set -e


#Save for backup our actual vimrc file
cp /etc/vim/vimrc /etc/vim/vimrc.back

#Add your own customizations 
try 
  cp awesome_vimrc ~/.vimrc
catch
endtry > /etc/vim/vimrc

source ~/.vimrc

echo "Installed the Ultimate Vim configuration successfully! Enjoy (°_°)"
